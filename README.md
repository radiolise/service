# Radiolise Web Service

## Developer API

### What is Radiolise?

Radiolise is a free/libre web application that allows you to play your favorite TV and radio streams. You can easily access the user interface and receive and play streams via your favorite web browser.

**[Try Radiolise](https://radiolise.com/)**

### About the API

The actual purpose of the web service is to provide additional information (such as now playing data) about a specific radio station to keep users of Radiolise up to date on current song names or headlines.

To make it easier for you to benefit from the API as well and to improve it, I have decided to offer it as free/libre software, too.

### How it works

To use the API, just provide a GET or POST request parameter named ‘url’ which contains the URL to the livestream itself.

You will get a JSON object containing the following keys if they are available:

- MIME type (‘content-type’)
- Station description (‘description’)
- Genre (‘genre’)
- Station name (‘name’)
- Now playing information (‘title’)
- Website (‘url’)

**Example w/ SWR3** (GET method): https://service.radiolise.com/?url=http://swr-swr3-live.cast.addradio.de/swr/swr3/live/mp3/128/stream.mp3

### Licensing

    © 2018-2020 Marco Bauer
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
