<?php
  $url = $_REQUEST["url"];
  $play = $_REQUEST["play"];;
  if (count($_REQUEST) == 0) {
    include_once("welcome.php");
    exit();
  } else if ((bool) $play) {
    header("HTTP/1.1 302 Found");
    header("location: $url");
    exit();
  }
  $result = null;
  $icy_metaint = -1;
  $needle = "StreamTitle=";
  $opts = array(
    "http" => array(
      "method" => "GET",
      "header" => "Icy-MetaData: 1"
    )
  );
  $default = stream_context_set_default($opts);
  $stream = fopen($url, "r");
  $headers = array(); 
  foreach ($http_response_header as $k => $v) {
    $t = explode(":", $v, 2);
    $headers[strtolower(trim($t[0]))] = trim($t[1]);
  }
  $icy_metaint = $headers["icy-metaint"];
  if($icy_metaint != -1) {
    $offset = $icy_metaint;
    while (!$result) {
      $buffer = stream_get_contents($stream, 300, $offset);
      if (strpos($buffer, $needle) !== false) {
        $title = explode($needle, $buffer);
        $title = trim($title[1]);
        $result = trim(substr($title, 1, strpos($title, "';") - 1));
        if (strpos($buffer, "adw_ad") === false) {
          break;
        }
      }
      else if ($offset == $icy_metaint) {
        break;
      }
      $offset += $icy_metaint;
    }
  }
  if ($stream) {
    fclose($stream);
  }
  if ($result === null) {
    $ch = curl_init();
    $url = parse_url($url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36");
    curl_setopt($ch, CURLOPT_URL, $url["scheme"] . "://" . $url["host"] . ":" . $url["port"] . "/7");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    $title = explode(",", curl_exec($ch))[6];
    $title = substr($title, 0, strpos($title, "<"));
    $result = $title ?: $result;
    curl_close();
  }
  $needle = "- text=\"";
  if (strpos($result, $needle) !== false) {
    $ihsplit = explode($needle, $result);
    $result = ($ihsplit[0] ? $ihsplit[0] . "- " : "") . substr($ihsplit[1], 0, strpos($ihsplit[1], "\" "));
  }
  header("Content-Type: application/json");
  header("Access-Control-Allow-Origin: *");
  function json($utf8encode = false, $titleencode = false) {
    global $headers, $result;
    $contenttype = $headers["content-type"];
    $description = $headers["icy-description"];
    $genre = $headers["icy-genre"];
    $name = $headers["icy-name"];
    $url = $headers["icy-url"];
    return json_encode((object) array_filter(array(
      "content-type" => $utf8encode ? utf8_encode($contenttype) : $contenttype,
      "description" => $utf8encode ? utf8_encode($description) : $description,
      "genre" => $utf8encode ? utf8_encode($genre) : $genre,
      "name" => $utf8encode ? utf8_encode($name) : $name,
      "title" => $titleencode ? utf8_encode($result) : $result,
      "url" => $utf8encode ? utf8_encode($url) : $url
    ))) ?: json(true, $utf8encode);
  }
  echo json();
?>
